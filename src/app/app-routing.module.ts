import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LolCreateComponent } from './lol-create/lol-create.component';
import { LolEditComponent } from './lol-edit/lol-edit.component';
import { LolListComponent } from './lol-list/lol-list.component';


const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'lol-list' },
  { path: 'lol-shipper', component: LolCreateComponent },
  { path: 'lol-list', component: LolListComponent },
  { path: 'lol-edit/:id', component: LolEditComponent }, 
  { path: '**', component: LolListComponent } 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
