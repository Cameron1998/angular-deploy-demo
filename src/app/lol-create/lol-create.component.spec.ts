import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LolCreateComponent } from './lol-create.component';

describe('LolCreateComponent', () => {
  let component: LolCreateComponent;
  let fixture: ComponentFixture<LolCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LolCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LolCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
