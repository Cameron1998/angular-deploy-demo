import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LolEditComponent } from './lol-edit.component';

describe('LolEditComponent', () => {
  let component: LolEditComponent;
  let fixture: ComponentFixture<LolEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LolEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LolEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
