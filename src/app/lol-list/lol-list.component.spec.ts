import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LolListComponent } from './lol-list.component';

describe('LolListComponent', () => {
  let component: LolListComponent;
  let fixture: ComponentFixture<LolListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LolListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LolListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
