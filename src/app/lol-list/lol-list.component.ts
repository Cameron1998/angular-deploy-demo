import { Component, OnInit } from '@angular/core';
import { LolChampion } from '../shared/lol-champion';
import { RestApiService } from '../shared/rest-api.service';

@Component({
  selector: 'app-lol-list',
  templateUrl: './lol-list.component.html',
  styleUrls: ['./lol-list.component.css']
})
export class LolListComponent implements OnInit {

  champions: any[] = [];

  constructor(private restApi: RestApiService) { }

  ngOnInit(): void {
    this.loadChampions();
  }

  loadChampions() {
    return this.restApi.getChampions().subscribe((data: LolChampion[]) => {
        this.champions = data;
    })
  }

  deleteLol(id:any) {
    if (window.confirm('Are you sure, you want to delete?')){
      this.restApi.deleteLol(id).subscribe(data => {
        this.loadChampions()
      })
    }
  } 
}
