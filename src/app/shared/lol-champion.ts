export class LolChampion {
    constructor() {
        this.id = 0;
        this.name = "";
        this.role = "";
        this.difficulty = "";
        this.imageUrl = "";
    }
    id: number;
    name: string;
    role: string;
    difficulty: string;
    imageUrl: string;
}
